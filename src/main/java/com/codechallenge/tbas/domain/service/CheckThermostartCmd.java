package com.codechallenge.tbas.domain.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.codechallenge.tbas.domain.model.ThermostatSingleton;
import com.codechallenge.tbas.util.DataToReadingRecordMapperUtils;
import com.codechallenge.tbas.util.GeneralConstantsUtils;
import com.codechallenge.tbas.util.JsonUtils;
import com.codechallenge.tbas.util.PropertyReaderUtils;
import com.codechallenge.tbas.util.TimestampConverterUtils;

public class CheckThermostartCmd implements Command {

	/**
	 * Check temperature more than red high
	 * 
	 * @author wang
	 *
	 */
	@Override
	public void execute() throws Exception {
		ThermostatSingleton thermostatSingleton = ThermostatSingleton.getInstance();
		int readingCount = Integer
				.parseInt(PropertyReaderUtils.getPropertyMap().get(GeneralConstantsUtils.READINGCOUNT));

		LocalDateTime currentRecordTime = DataToReadingRecordMapperUtils.getReadingRecord().getTimestamp();
		int satelliteId = DataToReadingRecordMapperUtils.getReadingRecord().getSatelliteId();
		double redHighLimit = DataToReadingRecordMapperUtils.getReadingRecord().getRedHighLimit();
		double rawValue = DataToReadingRecordMapperUtils.getReadingRecord().getRawValue();

		// if meet condition
		if (rawValue > redHighLimit) {
			if (thermostatSingleton.getReadingRecords().containsKey(satelliteId)) {
				thermostatSingleton.getReadingRecords().get(satelliteId).add(currentRecordTime);
			} else {
				ArrayList<LocalDateTime> list = new ArrayList<LocalDateTime>();
				list.add(currentRecordTime);
				thermostatSingleton.getReadingRecords().put(satelliteId, list);
				thermostatSingleton.getLeftPoint().put(satelliteId, 0);
			}

			List<LocalDateTime> localDateTimeList = thermostatSingleton.getReadingRecords().get(satelliteId);

			// exclude the left point outside the sliding window
			while (TimestampConverterUtils.compareTimestamp(
					localDateTimeList.get(thermostatSingleton.getLeftPoint().get(satelliteId)),
					currentRecordTime) > 0) {
				thermostatSingleton.getLeftPoint().put(satelliteId,
						thermostatSingleton.getLeftPoint().get(satelliteId) + 1);
			}

			// check if more than 3 records in the sliding window
			if (localDateTimeList.size() - thermostatSingleton.getLeftPoint().get(satelliteId) >= readingCount) {
				LocalDateTime localDateTime = localDateTimeList
						.get(thermostatSingleton.getLeftPoint().get(satelliteId));
				JsonUtils.getRedHighJson(localDateTime, satelliteId);
			}
		}
	}
}
