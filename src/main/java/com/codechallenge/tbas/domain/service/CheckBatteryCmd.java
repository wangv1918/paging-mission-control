package com.codechallenge.tbas.domain.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.codechallenge.tbas.domain.model.BatterySingleton;
import com.codechallenge.tbas.util.DataToReadingRecordMapperUtils;
import com.codechallenge.tbas.util.GeneralConstantsUtils;
import com.codechallenge.tbas.util.JsonUtils;
import com.codechallenge.tbas.util.PropertyReaderUtils;
import com.codechallenge.tbas.util.TimestampConverterUtils;

/**
 * Check battery less than red low
 * 
 * @author wang
 *
 */
public class CheckBatteryCmd implements Command {

	@Override
	public void execute() throws Exception {
		BatterySingleton batterySingleton = BatterySingleton.getInstance();
		int readingCount = Integer
				.parseInt(PropertyReaderUtils.getPropertyMap().get(GeneralConstantsUtils.READINGCOUNT));

		LocalDateTime currentRecordTime = DataToReadingRecordMapperUtils.getReadingRecord().getTimestamp();
		int satelliteId = DataToReadingRecordMapperUtils.getReadingRecord().getSatelliteId();
		double redLowLimit = DataToReadingRecordMapperUtils.getReadingRecord().getRedLowLimit();
		double rawValue = DataToReadingRecordMapperUtils.getReadingRecord().getRawValue();

		// if meet condition
		if (rawValue < redLowLimit) {
			if (batterySingleton.getReadingRecords().containsKey(satelliteId)) {
				batterySingleton.getReadingRecords().get(satelliteId).add(currentRecordTime);
			} else {
				ArrayList<LocalDateTime> list = new ArrayList<LocalDateTime>();
				list.add(currentRecordTime);
				batterySingleton.getReadingRecords().put(satelliteId, list);
				batterySingleton.getLeftPoint().put(satelliteId, 0);
			}

			List<LocalDateTime> localDateTimeList = batterySingleton.getReadingRecords().get(satelliteId);
			
			// exclude the left point outside the sliding window
			while (TimestampConverterUtils.compareTimestamp(localDateTimeList.get(batterySingleton.getLeftPoint().get(satelliteId)),
					currentRecordTime) > 0) {
				batterySingleton.getLeftPoint().put(satelliteId, batterySingleton.getLeftPoint().get(satelliteId) + 1);
			}

			// check if more than 3 records in the sliding window
			if (localDateTimeList.size() - batterySingleton.getLeftPoint().get(satelliteId) >= readingCount) {
				LocalDateTime ldt = localDateTimeList.get(batterySingleton.getLeftPoint().get(satelliteId));
				
				JsonUtils.getRedLowJson(ldt, satelliteId);
			}
		}
	}
}
