package com.codechallenge.tbas.domain.service;

import java.util.HashMap;
import java.util.Map;

import com.codechallenge.tbas.util.GeneralConstantsUtils;

/**
 * Set up commands map 
 * @author wang
 *
 */
public class Invoker {
	private Map<String, Command> commands = new HashMap<>();

	public Invoker() {
		commands.put(GeneralConstantsUtils.BATTERY, new CheckBatteryCmd());
		commands.put(GeneralConstantsUtils.THERMOSTAT, new CheckThermostartCmd());
	}

	public void executeCommand(String key) throws Exception {
		Command command = commands.get(key);
		if (command != null) {
			command.execute();
		} else {
			System.out.println("Invalid command");
		}
	}
}