package com.codechallenge.tbas.domain.service;

/**
 * Super class for Command pattern
 * 
 * @author wang
 *
 */
public interface Command {
	void execute() throws Exception;
}
