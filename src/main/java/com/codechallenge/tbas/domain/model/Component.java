package com.codechallenge.tbas.domain.model;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;

/**
 * Super class for singleton.
 * @author wang
 *
 */
public class Component {	
	protected Map<Integer, Integer> leftPoint; // To keep track of sliding window left pointer
	protected Map<Integer, List<LocalDateTime>> readingRecords; // To keep track of readings per satellite
		
	protected Component() {		
		leftPoint = new HashMap<>();
		readingRecords = new HashMap<>();
	}	

	public Map<Integer, List<LocalDateTime>> getReadingRecords() {
		return readingRecords;
	}	

	public  Map<Integer, Integer> getLeftPoint() {
		return leftPoint;
	}
}
