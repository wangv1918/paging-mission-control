package com.codechallenge.tbas.domain.model;

import java.time.LocalDateTime;

/**
 * Class holds one reading data.
 * @author wang
 *
 */
public class ReadingRecord {
	private LocalDateTime timestamp;
	private int satelliteId;
	private double redHighLimit;
	private double yellowHighLimit;
	private double yellowLowLimit;
	private double redLowLimit;
	private double rawValue;
	private String component;

	/**
	 * @return the timestamp
	 */
	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @return the satelliteId
	 */
	public int getSatelliteId() {
		return satelliteId;
	}

	/**
	 * @param satelliteId the satelliteId to set
	 */
	public void setSatelliteId(int satelliteId) {
		this.satelliteId = satelliteId;
	}

	/**
	 * @return the redHighLimit
	 */
	public double getRedHighLimit() {
		return redHighLimit;
	}

	/**
	 * @param redHighLimit the redHighLimit to set
	 */
	public void setRedHighLimit(double redHighLimit) {
		this.redHighLimit = redHighLimit;
	}

	/**
	 * @return the yellowHighLimit
	 */
	public double getYellowHighLimit() {
		return yellowHighLimit;
	}

	/**
	 * @param yellowHighLimit the yellowHighLimit to set
	 */
	public void setYellowHighLimit(double yellowHighLimit) {
		this.yellowHighLimit = yellowHighLimit;
	}

	/**
	 * @return the yellowLowLimit
	 */
	public double getYellowLowLimit() {
		return yellowLowLimit;
	}

	/**
	 * @param yellowLowLimit the yellowLowLimit to set
	 */
	public void setYellowLowLimit(double yellowLowLimit) {
		this.yellowLowLimit = yellowLowLimit;
	}

	/**
	 * @return the redLowLimit
	 */
	public double getRedLowLimit() {
		return redLowLimit;
	}

	/**
	 * @param redLowLimit the redLowLimit to set
	 */
	public void setRedLowLimit(double redLowLimit) {
		this.redLowLimit = redLowLimit;
	}

	/**
	 * @return the rawValue
	 */
	public double getRawValue() {
		return rawValue;
	}

	/**
	 * @param rawValue the rawValue to set
	 */
	public void setRawValue(double rawValue) {
		this.rawValue = rawValue;
	}

	/**
	 * @return the component
	 */
	public String getComponent() {
		return component;
	}

	/**
	 * @param component the component to set
	 */
	public void setComponent(String component) {
		this.component = component;
	}
}
