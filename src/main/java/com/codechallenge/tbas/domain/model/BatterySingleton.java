package com.codechallenge.tbas.domain.model;

/**
 * Singleton Battery model
 * @author wang
 *
 */
public class BatterySingleton extends Component {
	private static BatterySingleton instance = null;	

	private BatterySingleton() {
	}

	public static synchronized BatterySingleton getInstance() {
		if (instance == null) {
			instance = new BatterySingleton();
		}
		return instance;
	}
}
