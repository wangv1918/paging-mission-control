package com.codechallenge.tbas.domain.model;

/**
 * Singleton Thermostat model
 * @author wang
 *
 */
public class ThermostatSingleton extends Component {
	private static ThermostatSingleton instance = null;

	private ThermostatSingleton() {
	}

	public static synchronized ThermostatSingleton getInstance() {
		if (instance == null) {
			instance = new ThermostatSingleton();
		}
		return instance;
	}
}
