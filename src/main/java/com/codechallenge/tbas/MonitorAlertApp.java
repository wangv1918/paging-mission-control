package com.codechallenge.tbas;

import java.io.BufferedReader;
import java.io.FileReader;

import com.codechallenge.tbas.domain.service.Invoker;
import com.codechallenge.tbas.util.DataToReadingRecordMapperUtils;
import com.codechallenge.tbas.util.GeneralConstantsUtils;
import com.codechallenge.tbas.util.JsonUtils;
import com.codechallenge.tbas.util.PropertyReaderUtils;

/**
 * Class to run the application
 * @author wang
 *
 */
public class MonitorAlertApp {
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			System.out.println("Usage: java APPChecker <input_file_path>");
			System.exit(1);
		}

		String inputFilePath = args[0]; // input testing file by run:java -cp jar.file testingFile

		PropertyReaderUtils.loadPropertyFile(GeneralConstantsUtils.PROPERTYFILENAME);

		Invoker invoker = new Invoker();
		try (BufferedReader reader = new BufferedReader(new FileReader(inputFilePath))) {
			String line;
			while ((line = reader.readLine()) != null) {
				String[] inputParts = line.split("\\|");

				if (inputParts.length != 8) {
					System.out.println("Invalid input record. Line number: " + inputParts.length);
					continue;
				}

				DataToReadingRecordMapperUtils.mapReadingArrayToBean(inputParts);
				String record = DataToReadingRecordMapperUtils.getReadingRecord().getComponent();
                invoker.executeCommand(record);				
			}
			// Print result
			JsonUtils.print();
		}
	}
}