package com.codechallenge.tbas.util;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 * A standard set of time stamp utility class.
 * 
 * @author wang
 *
 */
public final class TimestampConverterUtils {
	private static DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyyMMdd HH:mm:ss.SSS");
	private static DateTimeFormatter outpuUTCFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

	/**
	 * Private constructor to prevent instantiation
	 */
	private TimestampConverterUtils() {
		throw new AssertionError();
	}

	/**
	 * Convert local time format to UTC format. UTC "Coordinated Universal Time" or
	 * "Zulu" time.
	 * 
	 * @param localDateTime
	 * @return UTC format timestamp
	 */
	public static String convertLocalDateTimeToUTC(LocalDateTime localDateTime) {
		String outputDatetime = "";

		try {
			outputDatetime = localDateTime.atOffset(ZoneOffset.UTC).format(outpuUTCFormatter);
		} catch (DateTimeParseException e) {
			throw e;
		}

		return outputDatetime;
	}

	/**
	 * Input two time stamp, compare the gap between them in minute
	 * 
	 * @param firstReadingTime
	 * @param currentReadingTime
	 * @return positive value if more than INTERVAL minutes between
	 */
	public static long compareTimestamp(LocalDateTime firstReadingTime, LocalDateTime currentReadingTime) {
		int t = Integer.parseInt(PropertyReaderUtils.getPropertyMap().get(GeneralConstantsUtils.READINGINTERVAL));
		return Duration.between(firstReadingTime, currentReadingTime).compareTo(Duration.ofMinutes(t));
	}

	/**
	 * Reads string and convert localDateTime
	 * 
	 * @param timestamp
	 * @return localDateTime
	 */
	public static LocalDateTime toLocalDateTime(String timestamp) {
		LocalDateTime localDateTime = LocalDateTime.parse(timestamp, inputFormatter);

		return localDateTime;
	}
}
