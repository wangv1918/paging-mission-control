package com.codechallenge.tbas.util;

import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Methods of Json Object utility class.
 * 
 * @author wang
 *
 */
public final class JsonUtils {
	private static int space = 4; // With four indent spaces
	private static JSONArray jsonArray = new JSONArray();

	/**
	 * Private constructor to prevent instantiation
	 * @return 
	 */
	private JsonUtils() {
		throw new AssertionError();
	}
	
	/**
	 * Set JSON object print in order
	 * @param printJsonObj
	 * @throws Exception
	 */
	public static void jsonInOrder(JSONObject printJsonObj) throws Exception {
		try {
			Field changeMap = printJsonObj.getClass().getDeclaredField("map");
			changeMap.setAccessible(true);
			changeMap.set(printJsonObj, new LinkedHashMap<>());
			changeMap.setAccessible(false);
		} catch (Exception e) {
			throw e;
		}
	}

	public static void getRedHighJson(LocalDateTime time, int satelliteId) throws Exception {
		JSONObject printJsonObj = new JSONObject();
		JsonUtils.jsonInOrder(printJsonObj);
		printJsonObj.put("satelliteId", satelliteId);
		printJsonObj.put("severity", "RED HIGH");
		printJsonObj.put("component", GeneralConstantsUtils.THERMOSTAT);
		printJsonObj.put("timestamp", TimestampConverterUtils.convertLocalDateTimeToUTC(time));
		jsonArray.put(printJsonObj);
	}
	
	public static void getRedLowJson(LocalDateTime time, int satelliteId) throws Exception {
		JSONObject printJsonObj = new JSONObject();
		JsonUtils.jsonInOrder(printJsonObj);
		printJsonObj.put("satelliteId", satelliteId);
		printJsonObj.put("severity", "RED LOW");
		printJsonObj.put("component", GeneralConstantsUtils.BATTERY);
		printJsonObj.put("timestamp", TimestampConverterUtils.convertLocalDateTimeToUTC(time));
		jsonArray.put(printJsonObj);
	}
	
	public static void print() {
		System.out.println(jsonArray.toString(space));
	}
}
