package com.codechallenge.tbas.util;

/**
 * General utility class for constants.
 * @author wang
 *
 */
public final class GeneralConstantsUtils {
	/**
	 * Private constructor to prevent instantiation
	 */
	private GeneralConstantsUtils() {
		throw new AssertionError();
	}

	public static final String TESTDAT = "data.txt";
	
	public static final String PROPERTYFILENAME = "application.properties";
	
	public static final String READINGCOUNT = "readings.count";
	
	public static final String READINGINTERVAL = "readings.interval";
	
	public static final String BATTERY = "BATT";
	
	public static final String THERMOSTAT = "TSTAT";
}
