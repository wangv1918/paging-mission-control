package com.codechallenge.tbas.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * A set of methods for property file.
 * 
 * @author wang
 *
 */
public class PropertyReaderUtils {
	private static Properties properties = new Properties();
	private static Map<String, String> propertyMap = new HashMap<>();

	/**
	 * Private constructor to prevent instantiation.
	 */
	private PropertyReaderUtils() {
		throw new AssertionError();
	}

	/**
	 * Reads a property file. 
	 * 
	 * @throws IOException
	 */
	public static void loadPropertyFile(String fileName) throws IOException {
		try (InputStream input = PropertyReaderUtils.class.getClassLoader().getResourceAsStream(fileName)) 
		{
			if (input == null) {
				throw new IOException("Unable to find properties file on classpath");
			}
			properties.load(input);
		} catch (IOException ex) {
			throw ex;
		}
	}
	
	/**
	 * Reads reach key:value from property file and returns a map.
	 * @return a Map contains property key and value
	 */
	public static Map<String, String> getPropertyMap(){
		for (String key : properties.stringPropertyNames()) {
			propertyMap.put(key, properties.getProperty(key));
		}
		
		return propertyMap;
	}
}
