package com.codechallenge.tbas.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.codechallenge.tbas.domain.model.ReadingRecord;

public final class DataToReadingRecordMapperUtils {
	/**
	 * First pipe delimited record index.
	 */
	private static final int DATE_TIME_INDEX = 0;

	/**
	 * Second pipe delimited record index.
	 */
	private static final int SATELLITE_ID_INDEX = 1;

	/**
	 * Third pipe delimited record index.
	 */
	private static final int RED_HIGH_IMIT_INDEX = 2;
	
	/**
	 * Fourth pipe delimited record index.
	 */
	private static final int YELLOW_HIGH_IMIT_INDEX = 3;
	
	/**
	 * Fifth pipe delimited record index.
	 */
	private static final int YELLOW_LOW_IMIT_INDEX = 4;
	
	/**
	 * Sixth pipe delimited record index.
	 */
	private static final int RED_LOW_IMIT_INDEX = 5;

	/**
	 *Seventh pipe delimited record index.
	 */
	private static final int RAW_VALUE_INDEX = 6;
	
	/**
	 * Eighth pipe delimited record index.
	 */
	private static final int COMPONENT_INDEX = 7;
	
	private static ReadingRecord readingRecord = new ReadingRecord();

	/**
	 * Get a reading data and map data to reading record model
	 * @param oneLineReadingData
	 * @throws IllegalArgumentException
	 * @throws ParseException
	 */
	public static void mapReadingArrayToBean(String[] oneLineReadingData)
			throws IllegalArgumentException, ParseException {
		if (oneLineReadingData.length <= 0) {
			throw new IllegalArgumentException("Reading record array can't be empty");
		}

		readingRecord.setTimestamp(TimestampConverterUtils.toLocalDateTime(oneLineReadingData[DATE_TIME_INDEX]));
		readingRecord.setSatelliteId(Integer.parseInt(oneLineReadingData[SATELLITE_ID_INDEX]));
		readingRecord.setRedHighLimit(Double.parseDouble(oneLineReadingData[RED_HIGH_IMIT_INDEX]));
		readingRecord.setYellowHighLimit(Double.parseDouble(oneLineReadingData[YELLOW_HIGH_IMIT_INDEX]));		
		readingRecord.setYellowLowLimit(Double.parseDouble(oneLineReadingData[YELLOW_LOW_IMIT_INDEX]));
		readingRecord.setRedLowLimit(Double.parseDouble(oneLineReadingData[RED_LOW_IMIT_INDEX]));
		readingRecord.setRawValue(Double.parseDouble(oneLineReadingData[RAW_VALUE_INDEX]));
		readingRecord.setComponent(oneLineReadingData[COMPONENT_INDEX].toString());
	}

	public static ReadingRecord getReadingRecord() {
		return readingRecord;
	}
}
