Design of Paging Mission Control Project
1) "paging-mission-control-project" is a Maven project. It developed using JavaSE-1.8.
2) The Singleton pattern is used to provide one instance for holding input data. BatterySingleton and ThermostatSingleton are 2 classes for separate battery data and temperature data.
3) The Command Pattern is applied to make the code open for extension, but closed for modification. For additional check, add a new class extends Command but keep same file reading code. CheckBatteryCmd is for checking battery voltage. CheckThermostatCmd is for checking temperature.
4) A sliding window algorithm is used to check last 5 minutes and 3 reading records.
5) Use appliation.properties file to modify the interval and count.


Import paging-mission-control-project to Eclipse or STS:
1) Open GitLab.
Click "Code" button. Copy URL.
2) From Eclipse or STS, File->import->Git->Project from Git (with smartimport). Click "Next" button.
3) Select Clone URI. Click "Next".
4) Paste URL on URL field. Click "Next".
5) Directory: Input your directory.
Remote name: origin. Click "Next".
6) Wait for it finished. Click "Finish".

 
How to run the application:
Pre-requirements: JavaSE-1.8 is installed on your local machine.
1) Verify project build path. 
From Eclipse or STS, right mouse click "paging-mission-control-project" project. Select "build Path"->click "Configure Build Path". The java Build Path will open. Then click "Libraries" tag. There should be two jars (JavaSe-1.8 and Maven Dependencies).
2) Build "paging-mission-control-project" project.
Right mouse click "paging-mission-control-project" project-> Run As->Click "Maven clean".
Right mouse click "paging-mission-control-project" again->Run As->Click "Maven Install".
3) Run application.
Right mouse click "MonitorAlertApp.java"->Run as->click "Run configure..". Run configuration window will show.
4) Click "Arguments" tab. On "Program arguments" input field: Paste testing file path, like "C:\development\temp\telemetrydata.txt". Then click "Run" button.
You can find 'telemetrydata.txt" under "paging-mission-control-project" -> "src/main/resources" folder.


